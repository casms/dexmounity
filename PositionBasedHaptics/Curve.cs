﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Profiling;

namespace UCL.CASMS.Haptics
{
    [Serializable]
    public class Curve
    {
        [Serializable]
        private struct Vertex
        {
            public Vector3 p;
            public float t;
        }

        [Serializable]
        private struct Line
        {
            public Vertex v0;
            public Vertex v1;

            public Vector3 GetPoint(float t)
            {
                return v0.p + (v1.p - v0.p) * t;
            }

            public float GetElement(float t)
            {
                return v0.t + (v1.t - v0.t) * t;
            }
        }

        [SerializeField]
        private List<Vertex> _vertices;

        [SerializeField]
        private List<Line> lines;

        public bool finished = false;

        private List<Vertex> vertices
        {
            get
            {
                if(_vertices == null)
                {
                    _vertices = new List<Vertex>();
                }
                return _vertices;
            }
        }

        // Unity does not support serialising generic types, so the BVH just maintains an index then we reference the actual Line in this inline class
        [Serializable]
        private class LineBVH : BVH
        {
            public void Initialise(IList<Line> lines)
            {
                this.lines = new List<Line>(lines);
                Initialise(MakeNodesForLines(this.lines));
            }

            public IEnumerable<Line> ClosestLine(Vector3 point)
            {
                foreach(var item in ClosestPrimitive(point))
                {
                    yield return lines[item];
                }
            }

            [SerializeField]
            private List<Line> lines;

            private static IEnumerable<LineBVH.Node> MakeNodesForLines(IList<Line> lines)
            {
                for (int i = 0; i < lines.Count; i++)
                {
                    var item = lines[i];
                    var position = (item.v1.p + item.v0.p) / 2f;
                    var radius = (item.v1.p - item.v0.p).magnitude / 2f;
                    yield return new LineBVH.Node(position, radius, i);
                }
            }
        }

        [SerializeField]
        private LineBVH bvh;

        public void Add(float t, Vector3 p)
        {
            vertices.Add(new Vertex()
            {
                p = p, t = t
            });
        }

        public void Finish()
        {
            // make the line

            lines = new List<Line>();

            for (int i = 0; i < vertices.Count - 1; i++)
            {
                lines.Add(new Line()
                {
                    v0 = vertices[i + 0],
                    v1 = vertices[i + 1]
                });
            }

            BuildBVH();

            finished = true;
        }

        private void BuildBVH()
        {
            bvh = new LineBVH();
            bvh.Initialise(lines);
        }

        public struct ClosestPointResult
        {
            /// <summary>
            /// Intersection point of the curve and collider in world space
            /// </summary>
            public Vector3 point;

            /// <summary>
            /// Normal of the surface at the Intersection ponit in world space
            /// </summary>
            public Vector3 normal;

            /// <summary>
            /// Parameter that will drive the endpoint to the Intersection Point position
            /// </summary>
            public float element;

            /// <summary>
            /// Convenience member to store the collided object if desired.
            /// </summary>
            public Collider collider;
        }

        public ClosestPointResult GetClosestPoint(Vector3 point)
        {
            Profiler.BeginSample("Closest Point Query");

            ClosestPointLineResult result;
            result.distance = float.MaxValue;
            result.line = new Line();
            result.t = 0;
            foreach (var item in bvh.ClosestLine(point))
            {
                var thisresult = GetClosestPointOnLine(point, item);
                if(thisresult.distance < result.distance)
                {
                    result = thisresult;
                }
            }

            ClosestPointResult finalresult;
            finalresult.point = result.line.GetPoint(result.t);
            finalresult.element = result.line.GetElement(result.t);
            finalresult.normal = Vector3.zero;
            finalresult.collider = null;

            Profiler.EndSample();

            return finalresult;
        }

        public Bounds GetBounds()
        {
            var v0 = lines.First().v0.p;
            var bounds = new Bounds(v0, Vector3.zero);
            foreach (var line in lines)
            {
                var v1 = (line.v1.p);
                bounds.Encapsulate(v1);
            }
            return bounds;
        }

        public Bounds GetBounds(Transform curvetransform)
        {
            var v0 = curvetransform.transform.TransformPoint(lines.First().v0.p);
            var bounds = new Bounds(v0, Vector3.zero);
            foreach (var line in lines)
            {
                var v1 = curvetransform.TransformPoint(line.v1.p);
                bounds.Encapsulate(v1);
            }
            return bounds;
        }

        public ClosestPointResult GetEntryPoint(Collider collider, Transform curvetransform)
        {
            Profiler.BeginSample("Curve Intersection Query");

            // walk the line

            RaycastHit hitinfo = new RaycastHit();
            ClosestPointResult result = new ClosestPointResult();

            var end = lines.Last();
            result.point = end.GetPoint(1f);
            result.element = end.GetElement(1f);

            foreach (var line in lines)
            {
                var v0 = curvetransform.TransformPoint(line.v0.p);
                var v1 = curvetransform.TransformPoint(line.v1.p);
                var origin = v0;
                var direction = v1 - v0;
                var length = direction.magnitude;

                if(collider.Raycast(new Ray(origin, direction), out hitinfo, length))
                {
                    var t = hitinfo.distance / length;

                    result.point = curvetransform.TransformPoint(line.GetPoint(t)); // point comes from line, so is in local space and must be transformed to world
                    result.normal = hitinfo.normal; // normal is already in world space
                    result.element = line.GetElement(t); // t is already in parameter space
                    break;               
                }
            }

            Profiler.EndSample();

            return result;
        }

        private struct ClosestPointLineResult
        {
            public Line line;
            public float t;
            public float distance;
        }

        private static ClosestPointLineResult GetClosestPointOnLine(Vector3 p, Line line)
        {
            var v0 = line.v0.p;
            var v1 = line.v1.p;
            var d = v1 - v0;
            var n = d.normalized;
            var l = d.magnitude;
            var t = Vector3.Dot(p - v0, n) / l;
            t = Mathf.Clamp(t, 0, 1);

            ClosestPointLineResult result;
            result.distance = (p - line.GetPoint(t)).magnitude;
            result.t = t;
            result.line = line;

            return result;
        }

        public void DebugDraw(Transform localSpace)
        {
            for (int i = 0; i < vertices.Count - 1; i++)
            {
                var v0 = localSpace.TransformPoint(vertices[i].p);
                var v1 = localSpace.TransformPoint(vertices[i + 1].p);

                Debug.DrawLine(v0, v1, Color.red);
            }
        }

        public IEnumerable<Vector3> GetPositions()
        {
            return vertices.Select(x => x.p);
        }
    }
}