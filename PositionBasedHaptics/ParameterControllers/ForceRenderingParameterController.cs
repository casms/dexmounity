﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UCL.CASMS.Haptics
{
    public class ForceRenderingParameterController : MonoBehaviour
    {
        private HapticParameterCurve Parameter;

        public Vector3 force;
        public bool overrideForce = false;

        public float position;
        public bool overridePosition = false;

        private void Awake()
        {
            Parameter = GetComponentInParent<HapticParameterCurve>();
        }

        // Update is called once per frame
        void Update()
        {
            if (!Parameter.CurveReady)
            {
                return;
            }

            var impulse = Vector3.zero;

            foreach(var sensor in GetComponentsInChildren<ForceRenderingSensor>())
            {
                impulse += sensor.Impulse;
            }

            if (!overrideForce)
            {
                force = impulse / Time.fixedDeltaTime;
            }
            var enable = force.magnitude > 0f;

            var param = Parameter.HandController.GetParameter(Parameter.Parameter);
            var current = param.Value;
            var torque = force.magnitude * 0.050f;

            var stiffness = -203.3f * (torque * torque * torque) + 163.8f * (torque * torque) + 1.826f * torque - 0.03613f;

            var direction = true;

            if (!overridePosition)
            {
                position = current + (direction ? -0.25f : 0.25f);
            }

            Parameter.HandController.SetHaptics(param, enable, position, direction, stiffness);

            if(enable)
            {
                //Debug.Log("f=" + force.magnitude + " " + "t=" + torque + " " + "a=" + stiffness + " " + "p0=" + current + " " + "p1=" + (current + 0.25f));
            }
        }
    }
}