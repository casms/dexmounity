﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UCL.CASMS.Haptics
{
    /// <summary>
    /// Computes the force applied to this collider during the frame based on the collision response. This force should be picked up by another component and used to drive the robot.
    /// </summary>
    public class ForceRenderingSensor : MonoBehaviour
    {
        [HideInInspector]
        public Vector3 Impulse;

        // Use this for initialization
        void Start()
        {
            Impulse = Vector3.zero;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawLine(transform.position, transform.position + Impulse);
        }

        private void LateUpdate()
        {
            // reset force for next frame, in LateUpdate so we know The HandForces script has run
            Impulse = Vector3.zero;
        }

        private void OnCollisionEnter(Collision collision)
        {

        }

        private void OnCollisionStay(Collision collision)
        {
            var impulse = collision.impulse;

            // dont know whats wrong with unitys integration (apart from lack of QA) but we need to do this:

            if (Vector3.Dot(collision.impulse.normalized, (transform.position - collision.collider.transform.position).normalized) < 0f)
            {
                impulse = -impulse; // impulses should always point *away* from the object they are trying to make the collider *de*penetrate. its not hard.
            }

            Impulse += impulse;
        }

        private void OnCollisionExit(Collision collision)
        {

        }
    }
}