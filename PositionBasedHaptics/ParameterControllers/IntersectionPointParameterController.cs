﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;

namespace UCL.CASMS.Haptics
{
    /// <summary>
    /// Computes haptic feedback parameters by finding the intersection point of the potential haptic trajectory
    /// with world geometry.
    /// </summary>
    public class IntersectionPointParameterController : MonoBehaviour, IHapticParameterController
    {
        public bool EnableForceReflection;

        private List<Collider> currentColliders = new List<Collider>();

        private HapticParameterCurve Parameter;

        private Vector3 force;
        private Vector3 position;
        private bool active;

        private void Reset()
        {
            var existing = GetComponentInParent<HapticParameterCurve>();
            if (existing != null && existing.CurveReady)
            {
                OnBuildLookupTable(existing);
            }
        }

        private void Awake()
        {
            Parameter = GetComponentInParent<HapticParameterCurve>();
        }

        // Use this for initialization
        void Start()
        {
            currentColliders.Clear();
        }

        void Update()
        {
            if(!Parameter.CurveReady)
            {
                return;
            }

            Profiler.BeginSample("Compute Intersection Point");

            Curve.ClosestPointResult finalresult;
            finalresult.element = float.MaxValue;
            finalresult.point = Vector3.zero;
            finalresult.normal = Vector3.one;
            finalresult.collider = null;

            foreach (var collider in currentColliders)   // let Unity handle the broadphase, picking up results with OnTriggerEnter/Exit
            {
                if (collider.GetComponent<HapticParameterCurve>() != null)
                {
                    continue;
                }

                var thisresult = Parameter.Curve.GetEntryPoint(collider, transform);
                thisresult.collider = collider;
                if (thisresult.element < finalresult.element)
                {
                    finalresult = thisresult;
                }
            }

            var param = Parameter.HandController.GetParameter(Parameter.Parameter);
            var value = finalresult.element;
            var enable = value < param.Range.Max;

            Profiler.EndSample();

            var stiffness = 1.5f;

            if (finalresult.collider != null)
            {
                if (finalresult.collider.sharedMaterial != null)
                {
                    stiffness = 1 - finalresult.collider.sharedMaterial.bounciness;
                }
            }

            Parameter.HandController.SetHaptics(param, enable, value, true, stiffness);

            // compute forces for collider

            if (EnableForceReflection)
            {
                if (finalresult.collider != null)
                {
                    var rb = finalresult.collider.GetComponent<Rigidbody>();
                    if (rb != null)
                    {
                        position = finalresult.point;
                        force = (Parameter.ChainEndpoint.transform.position - finalresult.point).normalized * (0.2016f*(stiffness*stiffness*stiffness) - 0.614f*(stiffness * stiffness) + 0.802f*stiffness - 0.04523f) * (Parameter.ChainEndpoint.transform.position - Parameter.transform.position).magnitude;
                        active = Vector3.Dot(force.normalized, finalresult.normal) < 0;

                        if (active)
                        {
                            rb.AddForceAtPosition(force, position);
                        }
                    }
                }
            }
        }

        public void OnDrawGizmos()
        {
            Debug.DrawRay(position, force, active ? Color.red : Color.gray);
        }

        public void OnBuildLookupTable(HapticParameterCurve Parameter)
        {
            var collider = GetComponent<BoxCollider>();
            if (collider == null)
            {
                collider = gameObject.AddComponent<BoxCollider>();
            }

            var rb = GetComponent<Rigidbody>();
            if (rb == null)
            {
                rb = gameObject.AddComponent<Rigidbody>();
            }

            //Use kinematic rigid body trigger colliders to detect collisions with everything, but affect nothing
            //https://docs.unity3d.com/Manual/CollidersOverview.html

            rb.isKinematic = true;
            rb.mass = 0;
            collider.isTrigger = true;

            var bounds = Parameter.Curve.GetBounds();

            collider.center = bounds.center;
            collider.size = bounds.size;
        }

        private void OnTriggerEnter(Collider other)
        {
            currentColliders.Add(other);
        }

        private void OnTriggerExit(Collider other)
        {
            currentColliders.Remove(other);
        }
    }
}