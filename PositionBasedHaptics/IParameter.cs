﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UCL.CASMS.Haptics
{
    [Serializable]
    public struct Range
    {
        public float Min;
        public float Max;

        public Range(float min, float max)
        {
            Min = min;
            Max = max;
        }
    }

    [Serializable]
    public abstract class Parameter
    {
        public float Value
        {
            get
            {
                return Get();
            }
            set
            {
                Set(value);
            }
        }

        public Range Range { get; protected set; }
        public string Name { get; protected set; }

        public abstract float Get();
        public abstract void Set(float value);

    }

    public interface IParameterList
    {
        IEnumerable<Parameter> Parameters { get; }
        Parameter GetParameter(string parameter);
    }


}