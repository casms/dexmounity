﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

namespace UCL.CASMS.Haptics
{
    [CustomEditor(typeof(HapticParameterCurve))]
    public class HapticParameterCurveInspector : Editor
    {
        private string[] options;
        private Dictionary<string, int> nameToOptionsIndex;

        void OnEnable()
        {
            var target = serializedObject.targetObject as HapticParameterCurve;
            var parameterlist = target.HandController as IParameterList;

            nameToOptionsIndex = new Dictionary<string, int>();

            int i = 0;
            foreach (var item in parameterlist.Parameters)
            {
                nameToOptionsIndex.Add(item.Name, i++);
            }

            options = nameToOptionsIndex.Keys.ToArray();
        }

        public override void OnInspectorGUI()
        {
            var target = serializedObject.targetObject as HapticParameterCurve;

            var selectedIndex = -1;
            if (target.Parameter != null && nameToOptionsIndex.ContainsKey(target.Parameter))
            {
                selectedIndex = nameToOptionsIndex[target.Parameter];
            }

            var newIndex = EditorGUILayout.Popup("Parameter", selectedIndex, options, EditorStyles.popup);

            if (newIndex >= 0)
            { 
                target.Parameter = options[newIndex];
            }
            else
            {
                target.Parameter = null;
            }

            serializedObject.Update();

            EditorGUILayout.PropertyField(serializedObject.FindProperty("HandController"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("ChainEndpoint"));

            serializedObject.ApplyModifiedProperties();

            if(GUILayout.Button("Build Lookup Table"))
            {
                target.BuildLookupTable();
            }
        }

        private void OnSceneGUI()
        {
            // for this issue: https://forum.unity.com/threads/drawgizmo-attribute-question.287092/    
        }

        [DrawGizmo(GizmoType.Active | GizmoType.InSelectionHierarchy)]
        static void DrawGizmos(HapticParameterCurve target, GizmoType gizmoType)
        {
            if (target.Curve != null)
            {
                Handles.color = Color.green;
                Handles.matrix = target.transform.localToWorldMatrix;
                Handles.DrawPolyLine(target.Curve.GetPositions().ToArray());
            }
        }
    }
}