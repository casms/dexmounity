﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace UCL.CASMS.Haptics
{
    /// <summary>
    /// Components should implement this in order to receive updates when the Curve is updated. Components do not need to implement this in order to use the curve.
    /// </summary>
    public interface IHapticParameterController
    {
        void OnBuildLookupTable(HapticParameterCurve parameter);
    }


    /// <summary>
    /// Manages a haptic device/control loop parameter based on a kinematic chain. This component should be placed at the root of the chain. 
    /// It will construct a 2D lookup table of the endpoint trajectory embedded in 3D space as a curve.
    /// Other components can use this to drive the haptic parameter based on the trajectory it contains.
    /// </summary>
    [ExecuteInEditMode]
    public class HapticParameterCurve : MonoBehaviour {

        public GameObject ChainEndpoint;
        public string Parameter;

        public bool drawCurve;

        private delegate Vector3 ForwardFunction(float value);

        public DexmoHandController HandController;

        [SerializeField]
        private Curve curve;

        public Curve Curve {
            get { return curve; }
            private set { curve = value; }
        }

        public bool CurveReady
        {
            get { return Curve != null && Curve.finished; }
        }

        private void Reset()
        {
            HandController = GetComponentInParent<DexmoHandController>();   // leaky abstraction!
            ChainEndpoint = transform.ChainEnd().gameObject;
        }

        /// <summary>
        /// Constructs the Curve based on the HandController. This is called from the custom Inspector.
        /// </summary>
        public virtual void BuildLookupTable()
        {
            var param = HandController.GetParameter(Parameter);

            // save the rest pose of this parameter so we can put it back. We don't know how changing this will affect the simulation.
            var paramvalue = param.Value;

            try
            {
                BuildLookupTable(
                (float v) =>
                {
                    param.Value = v; // the Parameter setter should cause the scene graph to update immediately...
                    return ChainEndpoint.transform.position; //...meaning this holds the right value
                }
                );
            }
            finally // dont ruin the rest pose if something goes wrong!
            {
                param.Value = paramvalue;
            }
        }

        private void BuildLookupTable(ForwardFunction function)
        {
            var param = HandController.GetParameter(Parameter);

            var min = param.Range.Min;
            var max = param.Range.Max;

            int steps = 128;

            Curve = new Curve();

            for (int i = 0; i < steps; i++)
            {
                var t = (float)i / (float)steps;
                var sample = function(min + t * (max - min));
                var local = transform.InverseTransformPoint(sample);
                Curve.Add(t, local);
            }

            Curve.Finish();

            foreach(var component in GetComponentsInChildren<IHapticParameterController>())
            {
                component.OnBuildLookupTable(this);
            }
        }

        

    }
}