﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;
using UCL.CASMS.DH;
using UCL.CASMS.Haptics;

public class DexmoModelController : DHModelController {

    [SerializeField]
    private Model model;

    private void OnEnable()
    {
        model = DexmoModel.Create();
    }

    public override void ResetModel()
    {
        model = DexmoModel.Create();
    }

    public override Model GetModel()
    {
        return model;
    }

    public void Save(string filename)
    {
        using (MemoryStream stream = new MemoryStream())
        {
            XmlSerializer serializer = new XmlSerializer(model.GetType());
            serializer.Serialize(stream, model);

            if (filename.Length > 0)
            {
                using (FileStream file = new FileStream(filename, FileMode.Create, System.IO.FileAccess.Write))
                {
                    file.Write(stream.GetBuffer(), 0, stream.GetBuffer().Length);
                }
            }
        }
    }
}
