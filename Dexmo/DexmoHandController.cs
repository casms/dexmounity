﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace UCL.CASMS.Haptics
{
    /// <summary>
    /// Implements IParameter for one of the controller variables in DexmoHandController.
    /// </summary>
    [Serializable]
    public class DexmoHandControllerParameter : Parameter
    {
        [SerializeField]
        private DexmoHandController instance;
        [SerializeField]
        private FieldInfo property;

        public Libdexmo.Model.FingerType finger;

        public DexmoHandControllerParameter(DexmoHandController instance, FieldInfo property)
        {
            this.instance = instance;
            this.property = property;

            Name = property.Name;

            Range range;
            var rangeAttribute = property.GetCustomAttributes(true).Where(x => (x as RangeAttribute) != null).FirstOrDefault() as RangeAttribute;
            range.Min = rangeAttribute.min;
            range.Max = rangeAttribute.max;
            Range = range;

            var fingerAttribute = property.GetCustomAttributes(true).Where(x => (x as DexmoDriverParameter) != null).FirstOrDefault() as DexmoDriverParameter;
            finger = fingerAttribute.finger;
        }

        public override float Get()
        {
            return (float)property.GetValue(instance);
        }

        public override void Set(float value)
        {
            property.SetValue(instance, value);
            instance.UpdateImmediate();
        }
    }

    public class DexmoDriverParameter : Attribute
    {
        public Libdexmo.Model.FingerType finger;

        public DexmoDriverParameter(Libdexmo.Model.FingerType finger)
        {
            this.finger = finger;
        }
    }

    /// <summary>
    /// Drives a HandModel instance using the normalised parameters from a Dexmo glove.
    /// The parameters here are normalised values between max Extensiona and max Flexion.
    /// </summary>
    [ExecuteInEditMode]
    public class DexmoHandController : MonoBehaviour, IParameterList
    {
        [NonSerialized]
        public Libdexmo.Client.Controller controller;

        /// <summary>
        /// Thumb in/out movement due to metacarpal abduction (cmc abduction)
        /// </summary>
        [Range(0,1)]
        public float ThumbRotation;

        /// <summary>
        /// Thumb up/down movement in the y-axis due to metacarpals rotating (cmc flex)
        /// </summary>
        [Range(0, 1)]
        public float ThumbSpread;

        /// <summary>
        /// Flex of the thumb
        /// </summary>
        [DexmoDriverParameter(Libdexmo.Model.FingerType.Thumb)]
        [Range(0, 1)]
        public float ThumbBend;

        [Range(0, 1)]
        public float IndexSpread;

        [DexmoDriverParameter(Libdexmo.Model.FingerType.Index)]
        [Range(0, 1)]
        public float IndexBend;

        [Range(0, 1)]
        public float MiddleSpread;

        [DexmoDriverParameter(Libdexmo.Model.FingerType.Middle)]
        [Range(0, 1)]
        public float MiddleBend;

        [Range(0, 1)]
        public float RingSpread;

        [DexmoDriverParameter(Libdexmo.Model.FingerType.Ring)]
        [Range(0, 1)]
        public float RingBend;

        [Range(0, 1)]
        public float LittleSpread;

        [DexmoDriverParameter(Libdexmo.Model.FingerType.Pinky)]
        [Range(0, 1)]
        public float LittleBend;

        public int ID { get; private set; }

        public delegate void DexmoConnectedHandler();
        public event DexmoConnectedHandler DexmoConnected;

        public int HapticUpdateRateLimit = 10;

        class HapticSettings
        {
            public bool[]  enabled     = new bool[5];
            public float[] position    = new float[5];
            public float[] stiffness   = new float[5];
            public bool[]  direction   = new bool[5];

            public void Set(Libdexmo.Model.FingerType finger, bool enabled, float stiffness, float rotation, bool direction)
            {
                this.enabled[(int)finger] = true;
                this.position[(int)finger] = rotation;
                this.stiffness[(int)finger] = enabled ? stiffness : 0f;
                this.direction[(int)finger] = direction;
            }

            public void ClearEnable()
            {
                for (int i = 0; i < 5; i++)
                {
                    this.enabled[i] = false;
                }
            }
        }

        private HapticSettings hapticSettings;

        private void Start()
        {
            ID = -1;
            hapticSettings = new HapticSettings();
        }

        public bool Initialised
        {
            get { return ID >= 0; }
        }

        [NonSerialized]
        private Dictionary<string, Parameter> parameters;

        public DexmoHandController()
        {
            parameters = new Dictionary<string, Parameter>();

            foreach (var field in GetType().GetFields())
            {
                foreach (var attribute in field.GetCustomAttributes(false))
                {
                    if (attribute is DexmoDriverParameter)
                    {
                        parameters.Add(field.Name, new DexmoHandControllerParameter(this, field));
                    }
                }
            }
        }

        public IEnumerable<Parameter> Parameters
        {
            get
            {
                return parameters.Values;
            }
        }

        public Parameter GetParameter(string parameter)
        {
            return parameters[parameter];
        }

        private float lastSetHapticsCall;

        public void SetHaptics(Parameter parameter, bool enabled, float rotation, bool direction, float stiffness)
        {
            var dexmoParameter = parameter as DexmoHandControllerParameter;
            if (hapticSettings != null) // if the component is disabled, just eat the call
            {
                hapticSettings.Set(dexmoParameter.finger, enabled, stiffness, rotation, direction);
            }
        }

        /// <summary>
        /// Updates all the joints with the parameters, and applies them immediately to the simulation.
        /// </summary>
        public void UpdateImmediate()
        {
            UpdateParameters();
            GetComponent<IDexmoPoseController>().UpdateImmediate();
        }

        void UpdateParameters()
        {
            if (GetComponent<IDexmoPoseController>() != null)
            {
                GetComponent<IDexmoPoseController>().UpdateParameters(
                    new DexmoHandParameters()
                    {
                        IndexBend = IndexBend,
                        IndexSpread = IndexSpread,
                        LittleBend = LittleBend,
                        LittleSpread = LittleSpread,
                        MiddleBend = MiddleBend,
                        MiddleSpread = MiddleSpread,
                        RingBend = RingBend,
                        RingSpread = RingSpread,
                        ThumbBend = ThumbBend,
                        ThumbRotation = ThumbRotation,
                        ThumbSpread = ThumbSpread
                    }
                    );
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (isActiveAndEnabled)
            {
                UpdateParameters();
            }

            if (Application.isPlaying)
            {
                if (controller == null)
                {
                    controller = new Libdexmo.Client.Controller();
                }

                var f = controller.GetFrame();
                if (f != null)
                {
                    var hand = controller.LatestHands.First().Value;

                    if (hand != null)
                    {
                        if (ID < 0)
                        {
                            ID = hand.DeviceAssignedId;
                            if (DexmoConnected != null)
                            {
                                DexmoConnected();
                            }
                        }

                        // get normalised parameters
                        IndexSpread = 1 - hand.IndexFinger.Joints[0].RotationNormalized[1];
                        IndexBend = hand.IndexFinger.Joints[0].RotationNormalized[2];

                        MiddleSpread = 1 - hand.MiddleFinger.Joints[0].RotationNormalized[1];
                        MiddleBend = hand.MiddleFinger.Joints[0].RotationNormalized[2];

                        RingSpread = 1 - hand.RingFinger.Joints[0].RotationNormalized[1];
                        RingBend = hand.RingFinger.Joints[0].RotationNormalized[2];

                        LittleSpread = 1 - hand.PinkyFinger.Joints[0].RotationNormalized[1];
                        LittleBend = hand.PinkyFinger.Joints[0].RotationNormalized[2];

                     //   ThumbRotation = hand.ThumbFinger.Joints[0].RotationNormalized[2];
                        ThumbSpread = hand.ThumbFinger.Joints[0].RotationNormalized[1];
                        ThumbBend = hand.ThumbFinger.Joints[1].RotationNormalized[2];
                    }
                }

                if (controller.Connected && ID >= 0)
                {
                    // temporary fix for control loop interrupt on set haptics. limit the update rate to a stable 10 hz. this means sethaptics must be called continously.
                    if (Time.time - lastSetHapticsCall > (1f / HapticUpdateRateLimit))
                    {
                        controller.ImpedanceControlFingers(ID, hapticSettings.enabled, hapticSettings.stiffness, hapticSettings.position, hapticSettings.direction);
                        hapticSettings.ClearEnable();
                        lastSetHapticsCall = Time.time;
                    }
                }
            }

        }


    }
}
