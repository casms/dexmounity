﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace UCL.CASMS.Haptics
{
    [CustomEditor(typeof(DexmoModelController))]
    public class DexmoModelNodeInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            DexmoModelController node = (DexmoModelController)target;

            if (GUILayout.Button("Reset"))
            {
                node.ResetModel();
            }

            if(GUILayout.Button("Save XML"))
            {
                var path = EditorUtility.SaveFilePanel(
                    "Save DH Model as XML",
                    "",
                    "model.xml",
                    "xml");

                node.Save(path);
            }

        }
    }
}