# README #

Support files for CASMS Dexmo Haptic Glove Unity Integration.

Add this repository as a submodule under Assets:

git submodule add git@bitbucket.org:casms/dexmounity.git Assets/DexmoUnity
